import { AUTHENTICATION_SUCCESS, LOGOUT } from "../Actions/actionTypes";
const initialState = {
  token: null,
  userId: null,
  username: null,
  userRole: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATION_SUCCESS:
      return {
        token: action.token,
        userId: action.userId,
        username: action.username,
        userRole: action.userRole,
      };
    case LOGOUT:
      return {
        token: null,
        userId: null,
        username: null,
        userRole: null,
      };
    default:
      return state;
  }
};

export default authReducer;

import { AUTHENTICATION_SUCCESS, LOGOUT } from "./actionTypes";

const authSuccess = (token, userId, username, userRole) => {
  localStorage.setItem("token", token);
  localStorage.setItem("username", username);
  localStorage.setItem("userId", userId);
  localStorage.setItem("userRole", userRole);
  return {
    type: AUTHENTICATION_SUCCESS,
    token,
    userId,
    username,
    userRole,
  };
};

const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("username");
  localStorage.removeItem("userId");
  localStorage.removeItem("userRole");
  return {
    type: LOGOUT,
  };
};

export { authSuccess, logout };

import React from "react";
import { fetchPostDetails } from "../../hackerNewsAPIs/postAPIs";
import { useHistory, useLocation } from "react-router-dom";
import _get from 'lodash/get';

import Loader from "react-loader-spinner";
import _map from "lodash/map";
import { useSelector } from "react-redux";
import "./post.css";
import {
  addComment,
  fetchPostComments,
} from "../../hackerNewsAPIs/commentsAPIs";
import CommentDetails from "../Comments/commentDetails";
import ShowPost from "./showPost";
import getRandomString from "../getRandomId";

function PostDetails(props) {
  const [post, setPost] = React.useState([]);
  const [isPostLoading, setIsPostLoading] = React.useState(true);
  const [isCommentsLoading, setIsCommentsLoading] = React.useState(true);
  const [comments, setComments] = React.useState([]);

  const [newComment, setNewComment] = React.useState("");
  const [isValidComment, setIsValidComment] = React.useState(false);

  const params = new URLSearchParams(props.location.search);
  const postId = params.get("id");

  const userId = useSelector((state) => state.auth.userId);
  const token = useSelector((state) => state.auth.token);
  const username = useSelector((state) => state.auth.username);

  const history = useHistory();
  const location = useLocation();
  function redirectToLogin() {
    if (!userId) {
      const pathname = location.pathname.substring(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : "?goto=" + pathname + location.search,
      });
      return true;
    }
    return false;
  }

  React.useEffect(() => {
    getPost();
    getPostComments();
  }, []);

  async function getPost() {
    fetchPostDetails(postId)
      .then((postResponse) => {
        setPost(postResponse);
        setIsPostLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function getPostComments() {
    fetchPostComments(postId)
      .then((commentsResponse) => {
        setComments(commentsResponse);
        setIsCommentsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function isUpvoted() {
    return userId ? post.upVotedBy.includes(userId) : false;
  }

  function onSubmitHandler(event) {
    event.preventDefault();
    if (!redirectToLogin()) {
      addComment({
        author: username,
        commentBody: newComment,
        commentId: getRandomString(6),
        createdOn: new Date(),
        points: 0,
        postId: postId,
        upVotedBy: [],
        userId: userId,
        token: token,
      })
        .then((addCommentResponse) => {
          setNewComment("");
          getPostComments();
          getPost();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  function onCommentInputChangeHandler(event) {
    event.preventDefault();
    const commentInput = event.target.value;
    setNewComment(commentInput);
    commentInput.length > 5
      ? setIsValidComment(true)
      : setIsValidComment(false);
  }
  return (
    <center>
      <table id="news-table" className="post-details-page-table">
        <tbody>
          {isPostLoading ? (
            <div id="post-detail-loader">
              <Loader type="ThreeDots" color="#2BAD60" height="50" width="50" />
            </div>
          ) : (
            <>
              <ShowPost
                postsData={post}
                serialNum={null}
                isUpvoted={isUpvoted}
                postPoints={_get(post,'points')}
                isLoading={isPostLoading}
              />
              <tr>
                <td colSpan="2"></td>
                <td>
                  <form onSubmit={onSubmitHandler}>
                    <textarea
                      cols="50"
                      rows="6"
                      id="add-comment-textarea"
                      onChange={onCommentInputChangeHandler}
                      value={newComment}
                    ></textarea>
                    <div id="add-comment-div">
                      <button
                        type="submit"
                        id="add-comment-button"
                        disabled={!isValidComment}
                      >
                        Add Comment
                      </button>
                    </div>
                  </form>
                </td>
              </tr>
            </>
          )}
        </tbody>
      </table>
      <table id="news-table" className="post-details-page-table">
        <tbody>
          {isCommentsLoading ? (
            <div id="comments-detail-loader">
              <Loader type="ThreeDots" color="#2BAD60" height="50" width="50" />
            </div>
          ) : (
            _map(comments, (commentData,index) => {
              const isCommentUpvoted = userId
                ? _get(commentData,'upVotedBy').includes(userId)
                : false;
              return (
                <CommentDetails
                  key={index+1}
                  commentData={commentData}
                  isUpvoted={isCommentUpvoted}
                  points={_get(commentData,'points')}
                />
              );
            })
          )}
        </tbody>
      </table>
    </center>
  );
}

export default PostDetails;

import React from "react";
import { fetchPosts } from "../../hackerNewsAPIs/postAPIs";
import ShowPosts from "./posts";
import "./post.css";

function NewPosts() {
  const postsType = "new";
  const [posts, setPosts] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    getPosts();
  }, []);

  async function getPosts() {
    fetchPosts(postsType)
      .then((postsResponse) => {
        setPosts(postsResponse);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log("Error while fetching posts: ", error);
      });
  }
  return <ShowPosts posts={posts} isLoading={isLoading} />;
}

export default NewPosts;

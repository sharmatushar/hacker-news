import React from "react";
import Loader from "react-loader-spinner";
import { useSelector } from "react-redux";
import _map from "lodash/map";
import _get from 'lodash/get';

import ShowPost from "./showPost";
import "./post.css";

function ShowPosts(props) {
  const userId = useSelector((state) => state.auth.userId);
  const { posts, isLoading } = props;
  return isLoading ? (
    <div className="loader">
      <Loader type="ThreeDots" color="#2BAD60" height="100" width="100" />
    </div>
  ) : (
    <center>
      <table id="news-table">
        <tbody>
          {_map(posts, (postsData,index) => {
            const isUpvoted = userId
              ? _get(postsData,'upVotedBy').includes(userId)
              : false;
            return (
              <ShowPost
                key={index+1}
                postsData={postsData}
                serialNum={index+1}
                isUpvoted={isUpvoted}
                postPoints={_get(postsData,'points')}
              />
            );
          })}
        </tbody>
      </table>
    </center>
  );
}

export default ShowPosts;

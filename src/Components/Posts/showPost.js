import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import _get from "lodash/get";

import { upvotePost } from "../../hackerNewsAPIs/postAPIs";
import CalculateDateDiff from "../calculateDateDiff";
import arrowImage from "../../assets/arrow.gif";
import ShowReported from "./showReported";
import "./post.css";

function ShowPost(props) {
  const userId = useSelector((state) => state.auth.userId);
  const authToken = useSelector((state) => state.auth.token);
  const { postsData, serialNum, isUpvoted, postPoints } = props;
  const [upvotedByUser, setUpvotedByUser] = React.useState(isUpvoted);
  const [points, setPoints] = React.useState(postPoints);

  const url =
    postsData.url.indexOf("http://") === -1 &&
    postsData.url.indexOf("https://") === -1
      ? "http://" + postsData.url
      : postsData.url;
  const domainName = new URL(url).hostname;

  const history = useHistory();
  const location = useLocation();

  function redirectToLogin() {
    if (!userId) {
      const pathname = location.pathname.substring(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : "?goto=" + pathname + location.search,
      });
      return true;
    }
    return false;
  }

  function upvotePostHandler() {
    if (!redirectToLogin()) {
      upvotePost(postsData.postId, userId, authToken)
        .then((upvoteResponse) => {
          setUpvotedByUser(true);
          setPoints((points) => points + 1);
        })
        .catch((error) => {
          console.log(error);
          setUpvotedByUser(false);
        });
    }
  }

  function revertUpvote() {
    if (!redirectToLogin()) {
      upvotePost(postsData.postId, userId, authToken)
        .then((upvoteResponse) => {
          setUpvotedByUser(false);
          setPoints((points) => points - 1);
        })
        .catch((error) => {
          console.log(error);
          setUpvotedByUser(true);
        });
    }
  }
  return (
    <>
      <tr>
        <td id="table-numbering">{serialNum ? serialNum + "." : null}</td>

        <td className="arrow-col">
          {userId && postsData.userId === userId ? (
            <span id="red-star-upvote">+</span>
          ) : upvotedByUser ? null : (
            <span className="upvote-image-link" onClick={upvotePostHandler}>
              <img id="arrow-image" src={arrowImage} alt=""></img>
            </span>
          )}
        </td>
        <td>
          <span>
            <a
              className="post-link"
              href={url}
              target="_blank"
              rel="noreferrer"
            >
              {_get(postsData, "title")}
            </a>
          </span>{" "}
          <span id="news-username">
            <a
              href={"http://" + domainName}
              id="news-username-link"
              target="_blank"
              rel="noreferrer"
            >
              <span>{domainName}</span>
            </a>
          </span>
        </td>
      </tr>
      <tr>
        <td colSpan="2"></td>
        <td id="news-username">
          <span>{points + " points | "}</span>
          <span>{" by "}</span>
          <Link
            id="news-username-link"
            to={{
              pathname: "/user",
              search: "?userId=" + postsData.userId,
            }}
          >
            <span> {_get(postsData, "author")}</span>
          </Link>
          <span>{" | "}</span>
          <CalculateDateDiff createdOn={postsData.createdOn} />
          <span>{" | "}</span>
          <ShowReported
            postId={_get(postsData, "postId")}
            reported={_get(postsData, "reported")}
            reportedBy={_get(postsData, "reportedBy")}
          />
          {upvotedByUser ? (
            <>
              <span>{" | "}</span>
              <span className="create-link" onClick={revertUpvote}>
                unvote
              </span>
            </>
          ) : null}
          <span>{" | hide | "}</span>
          <Link
            id="news-username-link"
            to={{
              pathname: "/post",
              search: "?id=" + _get(postsData, "postId"),
            }}
          >
            {_get(postsData, "comments") + " comments"}
          </Link>
        </td>
      </tr>
    </>
  );
}

export default ShowPost;

import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";

import { reportPost } from "../../hackerNewsAPIs/postAPIs";
import "./post.css";

function ShowReported(props) {
  const [isReported, setIsReported] = React.useState(false);
  const userId = useSelector((state) => state.auth.userId);
  const authToken = useSelector((state) => state.auth.token);
  const { postId, reportedBy } = props;
  const location = useLocation();
  const history = useHistory();
  function redirectToLogin() {
    if (!userId) {
      const pathname = location.pathname.substring(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : "?goto=" + pathname + location.search,
      });
      return true;
    }
    return false;
  }
  function reportPostHandler() {
    if (!redirectToLogin()) {
      reportPost(postId, userId, authToken)
        .then((response) => {
          setIsReported(true);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  return isReported ? (
    <span className="reported">[reported]</span>
  ) : (
    <span>
      {userId && reportedBy.includes(userId) ? (
        <span className="reported">[reported]</span>
      ) : (
        <span className="create-link" onClick={reportPostHandler}>
          [report]
        </span>
      )}
    </span>
  );
}

export default ShowReported;

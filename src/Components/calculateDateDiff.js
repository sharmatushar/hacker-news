function CalculateDateDiff(props) {
  const { createdOn } = props;
  const todayDate = new Date();
  const createdDate = new Date(createdOn);
  const diffTime = Math.abs(todayDate - createdDate);
  const diffDays = Math.round(diffTime / (1000 * 60 * 60 * 24));
  if (diffDays < 1) {
    const diffHours = Math.round(diffTime / (1000 * 60 * 60));
    if (diffHours < 1) {
      const diffMin = Math.round(diffTime / (1000 * 60));
      if (diffMin < 1) {
        const diffSec = Math.round(diffTime / 1000);
        if(diffSec===0) return <span>just now</span>;
        return <span>{diffSec + " second" + (diffSec===1 ? "" : "s") + " ago"}</span>;
      } 
      return <span>{diffMin + " minute" + (diffMin===1 ? "" : "s") + " ago"}</span>;
    }
    return <span>{diffHours + " hour" + (diffHours===1 ? "" : "s") + " ago"}</span>;
  } 
  return <span>{diffDays + " day" + (diffDays===1 ? "" : "s") + " ago"}</span>;
}
export default CalculateDateDiff;

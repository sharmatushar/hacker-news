import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import _get from "lodash/get";

import arrowImage from "../../assets/arrow.gif";
import CalculateDateDiff from "../calculateDateDiff";
import { upvoteComment } from "../../hackerNewsAPIs/commentsAPIs";
import "./comments.css";

function CommentDetails(props) {
  const { commentData, isUpvoted, points } = props;

  const userId = useSelector((state) => state.auth.userId);
  const authToken = useSelector((state) => state.auth.token);

  const [upvotedByUser, setUpvotedByUser] = React.useState(isUpvoted);
  const [commentPoints, SetCommentPoints] = React.useState(points);

  const history = useHistory();
  const location = useLocation();
  function redirectToLogin() {
    if (!userId) {
      const pathname = location.pathname.substring(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : "?goto=" + pathname + location.search,
      });
      return true;
    }
    return false;
  }

  function upvoteCommentHandler() {
    if (!redirectToLogin()) {
      upvoteComment(_get(commentData, "commentId"), userId, authToken)
        .then((response) => {
          SetCommentPoints((commentPoints) => commentPoints + 1);
          setUpvotedByUser(true);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }
  function revertUpvoteHandler() {
    if (!redirectToLogin()) {
      upvoteComment(_get(commentData, "commentId"), userId, authToken)
        .then((response) => {
          SetCommentPoints((commentPoints) => commentPoints - 1);
          setUpvotedByUser(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }
  return (
    <>
      <tr id="post-name-comments">
        <td id="col1-comments">
          {userId && _get(commentData, "userId") === userId ? (
            <span id="red-star-upvote">+</span>
          ) : upvotedByUser ? null : (
            <span className="upvote-image-link" onClick={upvoteCommentHandler}>
              <img id="arrow-image" src={arrowImage} alt=""></img>
            </span>
          )}
        </td>
        <td>
          <Link
            id="comments-username-link"
            to={{
              pathname: "/user",
              search: "?userId=" + _get(commentData, "userId"),
            }}
          >
            {commentData.author}
          </Link>

          <span>{" | " + commentPoints + " points | "}</span>
          <CalculateDateDiff createdOn={_get(commentData, "createdOn")} />
          <span>{" | "}</span>
          {upvotedByUser ? (
            <>
              <span className="create-link" onClick={revertUpvoteHandler}>
                unvote
              </span>
              <span>{" | "}</span>
            </>
          ) : null}
          <Link
            id="comments-username-link"
            to={{
              pathname: "/post",
              search: "?id=" + _get(commentData, "postId"),
            }}
          >
            Parent
          </Link>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>{_get(commentData, "commentBody")}</td>
      </tr>
      <tr className="additional-height-comments"></tr>
    </>
  );
}

export default CommentDetails;

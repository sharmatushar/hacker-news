import React from "react";
import _map from "lodash/map";
import _get from 'lodash/get';
import Loader from "react-loader-spinner";
import { useSelector } from "react-redux";

import "./comments.css";
import CommentDetails from "./commentDetails";

function ShowComments(props) {
  const { comments, isLoading } = props;
  const userId = useSelector((state) => state.auth.userId);
  return isLoading ? (
    <div className="loader">
      <Loader type="ThreeDots" color="#2BAD60" height="100" width="100" />
    </div>
  ) : (
    <center>
      <table id="news-table">
        <tbody>
          <tr className="additional-height-comments"></tr>
          {_map(comments, (commentData,index) => {
            const isUpvoted = userId
              ? _get(commentData,'upVotedBy').includes(userId)
              : false;
            return (
              <CommentDetails
                key={index+1}
                commentData={commentData}
                isUpvoted={isUpvoted}
                points={_get(commentData,'points')}
              />
            );
          })}
        </tbody>
      </table>
    </center>
  );
}

export default ShowComments;

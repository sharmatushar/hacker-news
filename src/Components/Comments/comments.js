import React from "react";

import { fetchComments } from "../../hackerNewsAPIs/commentsAPIs";
import ShowComments from "./showComments";
import "./comments.css";

function Comments() {
  const [comments, setComments] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  React.useEffect(() => {
    getComments();
  }, []);

  function getComments() {
    fetchComments()
      .then((commentsData) => {
        setComments(commentsData);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log("Error while fetching comments: ", error);
      });
  }
  return <ShowComments comments={comments} isLoading={isLoading} />;
}
export default Comments;

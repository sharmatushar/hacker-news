import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { useHistory, useLocation } from "react-router-dom";

import { addPost } from "../../hackerNewsAPIs/postAPIs";
import getRandomString from "../getRandomId";
import "./submit.css";

function Submit(props) {
  const [isUrlValid, setIsUrlValid] = React.useState(false);
  const [isValidForm, setIsValidForm] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [url, setUrl] = React.useState("");

  const userId = useSelector((state) => state.auth.userId);
  const authToken = useSelector((state) => state.auth.token);
  const username = useSelector((state) => state.auth.username);

  const isLoggedIn = useSelector((state) => state.auth.username !== null);
  const regex = new RegExp(
    /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
  );

  const history = useHistory();
  const location = useLocation();
  const pathname = location.pathname.substring(1);
  const redirectTo = {
    pathname: "/auth",
    search: pathname === "" ? "" : "?goto=" + pathname + location.search,
  };

  function redirectToLogin() {
    if (!userId) {
      history.push(redirectTo);
      return true;
    }
    return false;
  }
  function onSubmitHandler(event) {
    event.preventDefault();
    if (!redirectToLogin()) {
      const validUrl =
        url.indexOf("http://") === -1 && url.indexOf("https://") === -1
          ? "http://" + url
          : url;
      addPost({
        author: username,
        comments: 0,
        createdOn: new Date(),
        flagged: false,
        points: 0,
        postId: getRandomString(6),
        reported: false,
        reportedBy: [],
        title: title,
        upVotedBy: [],
        url: validUrl,
        userId: userId,
        token: authToken,
      })
        .then((responseData) => {
          history.push({
            pathname: "/new",
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  function onTitleInputChangeHandler(event) {
    event.preventDefault();
    const titleInput = event.target.value;
    setTitle(titleInput);
    titleInput.length > 5 && isUrlValid
      ? setIsValidForm(true)
      : setIsValidForm(false);
  }

  function onUrlInputChangeHandler(event) {
    event.preventDefault();
    const urlInput = event.target.value;
    setUrl(urlInput);
    const isValid = regex.test(urlInput); 
    setIsUrlValid(isValid);
    title.length > 5 && isValid ? setIsValidForm(true) : setIsValidForm(false);
  }

  return (
    <center>
      {isLoggedIn ? null : <Redirect to={redirectTo} />}

      <form onSubmit={onSubmitHandler}>
        <table id="submit-table">
          <tbody>
            <tr>
              <td className="submit-form-label">title</td>
              <td className="submit-form-input-cell">
                <input
                  className="submit-form-input"
                  type="text"
                  placeholder="Enter title"
                  onChange={onTitleInputChangeHandler}
                ></input>
              </td>
            </tr>
            <tr>
              <td className="submit-form-label">url</td>
              <td className="submit-form-input-cell">
                <input
                  className="submit-form-input"
                  type="text"
                  placeholder="Enter URL"
                  onChange={onUrlInputChangeHandler}
                ></input>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
                <button
                  id="submit-page-button"
                  type="submit"
                  disabled={!isValidForm}
                >
                  Submit
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </center>
  );
}
export default Submit;

import React from "react";
import "./auth.css";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import authenticate from "../../hackerNewsAPIs/authAPI";
import { authSuccess } from "../../Store/Actions/auth";

function Auth(props) {
  const [isValid, setIsValid] = React.useState(false);
  const [isSignIn, setIsSignIn] = React.useState(true);
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [error, setError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");

  const isLoggedIn = useSelector((state) => state.auth.username !== null);

  const params = new URLSearchParams(props.location.search);
  const redirectURL = params.get("goto");

  const dispatch = useDispatch();
  React.useEffect(() => {}, []);
  const changeAuthType = (event) => {
    event.preventDefault();
    setIsSignIn((isSignIn) => !isSignIn);
    setError(false);
  };

  function onSubmitHandler(event) {
    event.preventDefault();
    authenticate(username, password, isSignIn)
      .then((response) => {
        const token = response.token;
        const username = response.username;
        const userId = response.userId;
        const userRole = response.userRole;
        dispatch(authSuccess(token, userId, username, userRole));
      })
      .catch((error) => {
        setError(true);
        console.log(error.response);
        setErrorMessage(error?.response?.data?.message);
      });
  }

  function usernameChangeHandler(event) {
    const usernameInput = event.target.value;
    setUsername(usernameInput);
    if (usernameInput.length > 5 && password.length >= 6) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }

  function passwordChangeHandler(event) {
    const passwordInput = event.target.value;
    setPassword(passwordInput);
    if (passwordInput.length >= 6 && username.length > 5) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }

  return (
    <div className="master-div">
      {isLoggedIn ? (
        <Redirect to={redirectURL ? "/" + redirectURL : ""} />
      ) : null}
      <h2>Authentication</h2>
      <h4>You have to be logged in order to submit.</h4>
      <div id="error">
        {" "}
        {error
          ? isSignIn
            ? "Incorrect username or password"
            : errorMessage
          : null}
      </div>
      <form onSubmit={onSubmitHandler}>
        <div className="label">username</div>
        <input
          type="text"
          onChange={usernameChangeHandler}
          value={username}
          className="input"
        ></input>
        <div className="label">password</div>
        <input
          type="password"
          onChange={passwordChangeHandler}
          value={password}
          className="input"
        ></input>
        <div>
          <button
            type="submit"
            disabled={!isValid}
            className={"button button1 " + (isValid ? "" : "disableButton")}
          >
            {isSignIn ? "SIGN IN" : "SIGN UP"}
          </button>
          <button onClick={changeAuthType} className="button button2">
            {isSignIn ? "SWITCH TO SIGN UP" : "SWITCH TO SIGN IN"}
          </button>
        </div>
      </form>
    </div>
  );
}

export default Auth;

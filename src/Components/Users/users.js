import React from "react";
import Loader from "react-loader-spinner";
import _map from "lodash/map";
import _get from "lodash/get";
import { useSelector } from "react-redux";

import { months } from "../../resources/months";
import { fetchUserPosts } from "../../hackerNewsAPIs/postAPIs";
import { fetchUser } from "../../hackerNewsAPIs/userAPIs";
import ShowPost from "../Posts/showPost";
import "./users.css";

function UserDetails(props) {
  const [user, setUser] = React.useState({});
  const [showAllPosts, setShowAllPosts] = React.useState(false);
  const [postsLoading, setPostsLoading] = React.useState(true);
  const [posts, setPosts] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  const params = new URLSearchParams(props.location.search);
  const userId = params.get("userId");

  const currentUserId = useSelector((state) => state.auth.userId);

  React.useEffect(() => {
    fetchUser(userId)
      .then((userData) => {
        setUser(userData);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
    setShowAllPosts(false);
  }, [userId]);

  function getUserPosts() {
    fetchUserPosts(userId)
      .then((postsResponse) => {
        setPosts(postsResponse);
        setPostsLoading(false);
        if (postsResponse.length > 0) {
          setShowAllPosts(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function showUserPosts() {
    getUserPosts();
  }
  function hideUserPosts() {
    setShowAllPosts(false);
  }

  return isLoading ? (
    <div className="loader">
      <Loader type="ThreeDots" color="#2BAD60" height="100" width="100" />
    </div>
  ) : (
    <center>
      <table id="news-table">
        <tbody>
          <tr className="additional-height-user"></tr>
          <tr>
            <td className="user-details-label">name:</td>
            <td>{_get(user, "username")}</td>
          </tr>
          <tr>
            <td className="user-details-label">created:</td>
            <td>
              <DisplayCreatedOn createdOn={_get(user, "createdOn")} />
            </td>
          </tr>

          <tr className="additional-height-user"></tr>
        </tbody>
        <tbody>
          <tr>
            <td colSpan="2">
              {!showAllPosts ? (
                <span id="show-user-posts-toggle" onClick={showUserPosts}>
                  See all{" "}
                  {currentUserId === _get(user, "userId")
                    ? "your"
                    : _get(user, "username")}{" "}
                  posts
                </span>
              ) : (
                <span id="show-user-posts-toggle" onClick={hideUserPosts}>
                  Hide all{" "}
                  {currentUserId === _get(user, "userId")
                    ? "your"
                    : _get(user, "username")}{" "}
                  posts
                </span>
              )}
            </td>
          </tr>
        </tbody>
      </table>
      <table id="posts-table">
        <tbody>
          {!showAllPosts ? null : postsLoading ? (
            <tr>
              <td>...Loading</td>
            </tr>
          ) : (
            _map(posts, (postsData, index) => {
              const isUpvoted = userId
                ? _get(postsData, "upVotedBy").includes(userId)
                : false;
              return (
                <ShowPost
                  key={index + 1}
                  postsData={postsData}
                  serialNum={index + 1}
                  isUpvoted={isUpvoted}
                  postPoints={_get(postsData, "points")}
                />
              );
            })
          )}
        </tbody>
      </table>
    </center>
  );
}

function DisplayCreatedOn(props) {
  const { createdOn } = props;
  const createdDate = new Date(createdOn);
  const date = createdDate.getDate();
  const month = months[createdDate.getMonth()];
  const year = createdDate.getFullYear();
  return <span>{month + " " + date + ", " + year}</span>;
}
export default UserDetails;

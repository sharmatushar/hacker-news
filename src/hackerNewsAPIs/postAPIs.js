import axios from "axios";
import _get from 'lodash/get'

import { baseURL } from "../resources/baseURL";

export async function fetchPosts(postType) {
  const postsPromise = await axios.get(`${baseURL}/posts/${postType}`);
  const postsData = _get(postsPromise,'data');
  return postsData;
}

export async function fetchPostDetails(postId) {
  const postPromise = await axios.get(`${baseURL}/post/${postId}`);
  const postData = _get(postPromise,'data');
  return postData;
}

export async function fetchUserPosts(userId){
  const url = `${baseURL}/posts/user/${userId}/false`;
  const postsPromise = await axios.get(url);
  const postsData = _get(postsPromise,'data');
  return postsData;
}

export async function reportPost(postId, userId, token) {
  const url = `${baseURL}/post/report/${postId}/${userId}`;
  const authToken = "Bearer " + token;
  const reportPromise = await axios.put(url, null, {
    headers: {
      Authorization: authToken,
    },
  });
  const reportData = _get(reportPromise,'data');
  return reportData;
}

export async function upvotePost(postId, userId, token) {
  const url = `${baseURL}/post/vote/${postId}/${userId}`;
  const authToken = "Bearer " + token;
  const upvotePromise = await axios.put(url, null, {
    headers: {
      Authorization: authToken,
    },
  });
  const upvoteData = _get(upvotePromise,'data');
  return upvoteData;
}

export async function addPost(props) {
  const { token } = props;
  const { author } = props;
  const { comments } = props;
  const { createdOn } = props;
  const { flagged } = props;
  const { points } = props;
  const { postId } = props;
  const { reported } = props;
  const { reportedBy } = props;
  const { title } = props;
  const { upVotedBy } = props;
  const { url: postUrl } = props;
  const { userId } = props;

  const url = `${baseURL}/post`;
  const authToken = "Bearer " + token;
  const requestBody = {
    author,
    comments,
    createdOn,
    flagged,
    points,
    postId,
    reported,
    reportedBy,
    title,
    upVotedBy,
    url: postUrl,
    userId,
  };
  const addPostPromise = await axios.post(url, requestBody, {
    headers: {
      Authorization: authToken,
    },
  });
  const addPostResponse = _get(addPostPromise,'data');
  return addPostResponse;
}

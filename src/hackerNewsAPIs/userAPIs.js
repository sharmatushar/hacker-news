import axios from "axios";
import { baseURL } from "../resources/baseURL";

export async function fetchUser(userId) {
  const userPromise = await axios.get(`${baseURL}/user/${userId}`);
  const userData = await userPromise.data;
  return userData;
}

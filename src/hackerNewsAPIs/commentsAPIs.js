import axios from "axios";
import _get from 'lodash/get'

import { baseURL } from "../resources/baseURL";

export async function fetchComments() {
  const commentsPromise = await axios.get(`${baseURL}/comments/new`);
  const commentsData = _get(commentsPromise,'data');
  return commentsData;
}

export async function fetchPostComments(postId) {
  const commentsPromise = await axios.get(`${baseURL}/comments/post/${postId}`);
  const commentsData = _get(commentsPromise,'data');
  return commentsData;
}

export async function addComment(props) {
  const { author } = props;
  const { commentBody } = props;
  const { commentId } = props;
  const { createdOn } = props;
  const { points } = props;
  const { postId } = props;
  const { upVotedBy } = props;
  const { userId } = props;
  const { token } = props;

  const url = `${baseURL}/comment`;
  const authToken = "Bearer " + token;
  const requestBody = {
    author,
    commentBody,
    commentId,
    createdOn,
    points,
    postId,
    upVotedBy,
    userId,
  };
  const addCommentPromise = await axios.post(url, requestBody, {
    headers: {
      Authorization: authToken,
    },
  });
  const addCommentResponse = _get(addCommentPromise,'data');
  return addCommentResponse;
}

export async function upvoteComment(commentId, userId, token) {
  const url = `${baseURL}/comment/vote/${commentId}/${userId}`;
  const authToken = "Bearer " + token;
  const upvotePromise = await axios.put(url, null, {
    headers: {
      Authorization: authToken,
    },
  });
  const upvoteData = _get(upvotePromise,'data');
  return upvoteData;
}

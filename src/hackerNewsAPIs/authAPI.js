import axios from "axios";
import _get from 'lodash/get'
import { baseURL } from "../resources/baseURL";
async function authenticate(username, password, isSignIn) {
  const url = baseURL + (isSignIn ? "/login" : "/signup");
  const requestBody = {
    username,
    password,
  };
  const authPromise = await axios.post(url, requestBody);
  const authData = _get(authPromise,'data');
  return authData;
}

export default authenticate;

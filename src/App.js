import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import { authSuccess } from './Store/Actions/auth';


import Nav from './/Navbar/Nav'
import Welcome from './Components/hackerNewsWelcome'
import NewPosts from './Components/Posts/newPosts';
import Comments from './Components/Comments/comments'
import UserDetails from './Components/Users/users';
import Submit from './Components/Submit/submit';
import Auth from './Components/Auth/auth';
import Logout from './Components/Auth/logout';
import TopPosts from './Components/Posts/topPosts';
import PostDetails from './Components/Posts/postDetails';

function App() {
  const dispatch = useDispatch();
  function CheckIfLoggedin(){
    const token = localStorage.getItem("token");
    if(token){
      const userId = localStorage.getItem("userId");
      const username = localStorage.getItem("username");
      const userRole = localStorage.getItem("userRole");
      dispatch(authSuccess(token, userId, username, userRole));
    }
  }
  React.useEffect(()=>{
    CheckIfLoggedin();
  },[]);

  return (
    <Router>
      <div>
        <Nav />
          <Switch>
            <Route path="/" exact component={TopPosts}/>
            <Route path="/top" exact component={TopPosts}/>
            <Route path="/welcome" component={Welcome}/>
            <Route path="/new" exact component={NewPosts}/>
            <Route path="/comments" component={Comments}/>
            <Route path="/submit" component={Submit}/>
            <Route path='/auth' component={Auth}/>
            <Route path='/signout' component={Logout}/>
            <Route path="/user" component={UserDetails}/>
            <Route path="/post" component={PostDetails}/>
          </Switch>
      </div>
    </Router>
  );
}

export default App;

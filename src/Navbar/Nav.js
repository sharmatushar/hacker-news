import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

import "./Nav.css";
import logo from "../assets/y18.gif";

function Nav() {
  const username = useSelector((state) => state.auth.username);
  const userId = useSelector((state) => state.auth.userId);
  const location = useLocation();
  const pathname = location.pathname.substring(1);

  return (
    <table id="navbar-table">
      <tbody>
        <tr>
          <td id="hacker-news-logo">
            <Link to="/top">
              <img src={logo} id="hacker-news-image" alt="" />
            </Link>
          </td>
          <td>
            <Link to="/top" className="nav-links">
              <span id="hacker-news-heading">Hacker News</span>
            </Link>
            <span className="navbar-item-separator">|</span>
            <Link className="nav-links" to="/new">
              <span className="navbar-items">new</span>
            </Link>
            <span className="navbar-item-separator">|</span>
            <Link className="nav-links" to="/comments">
              <span className="navbar-items">comments</span>
            </Link>
            <span className="navbar-item-separator">|</span>
            <Link className="nav-links" to="/submit">
              <span className="navbar-items">submit</span>
            </Link>
          </td>
          <td id="last-item">
            {username == null ? (
              <Link
                className="nav-links"
                to={{
                  pathname: "/auth",
                  search:
                    (pathname === "auth" || pathname === ""
                      ? ""
                      : "?goto=" + pathname) + location.search,
                }}
              >
                <span>login</span>
              </Link>
            ) : (
              <>
                <Link
                  className="nav-links"
                  to={{
                    pathname: "/user",
                    search: "?userId=" + userId,
                  }}
                >
                  <span>{username}</span>
                </Link>
                <span className="navbar-item-separator">|</span>
                <Link
                  className="nav-links"
                  to={{
                    pathname: "/signout",
                    search: "?goto=" + pathname + location.search,
                  }}
                >
                  <span>logout</span>
                </Link>
              </>
            )}
          </td>
        </tr>
      </tbody>
    </table>
  );
}
export default Nav;
